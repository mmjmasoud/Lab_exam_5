<?php

namespace App;

class Student
{
    public $id = '',
            $first_name = '',
            $middle_name = '',
            $last_name = '',
            $conn;

    public function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', '', 'examl');
    }

    public function prepare($data)
    {
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('first_name', $data)) {
            $this->first_name = $data['first_name'];
        }
        if(array_key_exists('middle_name', $data)) {
            $this->middle_name = $data['middle_name'];
        }
        if(array_key_exists('last_name', $data)) {
            $this->last_name = $data['last_name'];
        }
    }

    public function index()
    {
        $_allStudents = array();
        $query = "SELECT * FROM `students`";
        $result = mysqli_query($this->conn, $query);
        while($row = mysqli_fetch_assoc($result)) {
            $_allStudents[] = $row;
        }

        return $_allStudents;
    }

    public function view()
    {
        $query = "SELECT * FROM `students` WHERE `id` = " . $this->id;

        $result = mysqli_query($this->conn, $query);

        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    public function store()
    {
        $query = "INSERT INTO `students` (`first_name`, `middle_name`, `last_name`) VALUES ('". $this->first_name ."', '". $this->middle_name ."', '". $this->last_name ."');";

        if(mysqli_query($this->conn, $query)) {
            Message::message("Data has been successfully stored");
            header('Location: index.php');
        }
    }

    public function update()
    {
        $query = "UPDATE `examl`.`students` SET `first_name` = '". $this->first_name ."', `middle_name` = '". $this->middle_name ."', `last_name` = '". $this->last_name ."' WHERE `students`.`id` = " . $this->id;

        if(mysqli_query($this->conn, $query)) {
            Message::message("Data has been successfully Updated");
            header('Location: index.php');
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `examl`.`students` WHERE `students`.`id` =" . $this->id;
        if(mysqli_query($this->conn, $query)) {
            Message::message("Data has been successfully Deleted");
            header('Location: index.php');
        }
    }
}