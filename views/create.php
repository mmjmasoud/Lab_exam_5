<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Books</title>

    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
</head>
<body>

<div class="container">

    <div class="col-md-10 col-md-offset-1">
        <nav>
            <div class="nav-bar">
                <a href="index.php" class="btn btn-primary">Home</a>
            </div>
        </nav>
        <h1>Create Student</h1>

        <form action="store.php" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="first_name" placeholder="First name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="middle_name" placeholder="Middle name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="last_name" placeholder="Last name">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>

    </div>
</div>

</body>
</html>

