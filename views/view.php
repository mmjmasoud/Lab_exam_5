<?php
include_once '../vendor/autoload.php';

use \App\Student;

$student = new Student();

$student->prepare($_GET);

$stdn = $student->view();

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Books</title>

    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
</head>
<body>



<div class="container">

    <div class="col-md-10 col-md-offset-1">
        <nav>
            <div class="nav-bar">
                <a href="index.php" class="btn btn-primary">Home</a>
            </div>
        </nav>

        <h1>Student</h1>

        <ul class="list-group">
            <li class="list-group-item">ID: <?= $stdn['id'] ?></li>
            <li class="list-group-item">First Name: <?= $stdn['first_name'] ?></li>
            <li class="list-group-item">Middle Name: <?= $stdn['middle_name'] ?></li>
            <li class="list-group-item">Last Name: <?= $stdn['last_name'] ?></li>
        </ul>

    </div>
</div>

</body>
</html>