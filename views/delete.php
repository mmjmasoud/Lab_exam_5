<?php

include_once '../vendor/autoload.php';

use \App\Student;


$student = new Student();

$student->prepare($_GET);

$student->delete();