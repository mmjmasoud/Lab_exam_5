<?php

include_once '../vendor/autoload.php';

use \App\Student;


$student = new Student();

$student->prepare($_POST);

$student->store();