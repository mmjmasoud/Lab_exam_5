<?php
include_once '../vendor/autoload.php';
session_start();
use App\Student;
use App\Message;

$student = new Student();

$allStudents = $student->index();

if(array_key_exists('message', $_SESSION)) {
    $message = Message::message();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Books</title>

    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    
    <div class="col-md-10 col-md-offset-1">

        <h1>All Students</h1>
        <p>
            <a href="create.php" class="btn btn-primary">Create New Student</a>
        </p>
        <?php if(!empty($message)) : ?>
            <div id="message" class="alert alert-success" role="alert">
                <?php echo $message;  ?>
            </div>
        <?php endif; ?>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th class="actions">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl = 0;
            foreach($allStudents as $student):
                $sl++; ?>
                <tr>
                    <td><?= $sl ?></td>
                    <td><?= $student['id'] ?></td>
                    <td><?= $student['first_name'] ?></td>
                    <td><?= $student['middle_name'] ?></td>
                    <td><?= $student['last_name'] ?></td>
                    <td class="actions">
                        <a href="view.php?id=<?= $student['id'] ?>" class="btn btn-primary">View</a>
                        <a href="edit.php?id=<?= $student['id'] ?>" class="btn btn-primary">Edit</a>
                        <a href="delete.php?id=<?= $student['id'] ?>" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>

</body>
</html>
