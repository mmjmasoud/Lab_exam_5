<?php
include_once '../vendor/autoload.php';

use \App\Student;

$student = new Student();

$student->prepare($_GET);

$stdn = $student->view();


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Books</title>

    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">
</head>
<body>



<div class="container">

    <div class="col-md-10 col-md-offset-1">
        <nav>
            <div class="nav-bar">
                <a href="index.php" class="btn btn-primary">Home</a>
            </div>
        </nav>

        <h1>Create Student</h1>

        <form action="update.php" method="post">
            <input type="hidden" name="id" value="<?= $stdn['id'] ?>">
            <div class="form-group">
                <input type="text" class="form-control" name="first_name" placeholder="First name" value="<?= $stdn['first_name'] ?>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="middle_name" placeholder="Middle name" value="<?= $stdn['middle_name'] ?>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="last_name" placeholder="Last name" value="<?= $stdn['last_name'] ?>">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Update">
            </div>
        </form>

    </div>
</div>

</body>
</html>

